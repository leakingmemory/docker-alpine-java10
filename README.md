Docker context directory under CC0 license. No restrictions to the extent I
am allowed to grant. - Jan-Espen Oversand

Docker build directory for Oracle JDK 10. You need to
download the jdk (jdk-10_linux-x64_bin.tar.gz) and make sure
you are in good standing with Oracle (license agreements and stuff)

docker build -t alpine-java10 .

